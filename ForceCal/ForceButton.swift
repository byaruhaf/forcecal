//
//  ForceButton.swift
//  ForceCal-uikit
//
//  Created by Franklin Byaruhanga on 28/08/2020.
//

import Foundation
import UIKit
import SwiftUI

class ForceButton: UIButton {
    var forceVal:CGFloat = 0.0
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        handleForceWithTouches(touches)
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        handleForceWithTouches(touches)
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        handleForceWithTouches(touches)
    }

    func handleForceWithTouches(_ touches: Set<UITouch>) {
        if touches.count != 1 {
//            print(touches.count)
            return
        }
        guard let touch = touches.first else {
//            print("First Touch")
            return
        }
        if self.traitCollection.forceTouchCapability == .available {
            forceVal = touch.force
//            print("touch_force = \(touch.force)")
        }
    }


}
