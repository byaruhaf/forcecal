//
//  ContentView.swift
//  ForceCal
//
//  Created by Franklin Byaruhanga on 28/08/2020.
//  Copyright © 2020 Franklin Byaruhanga. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var forceValue: CGFloat = 0.0
    @State var pct: CGFloat = 0
    var body: some View {
        VStack {
            Text("Force Touch Counter")
            Text("\(pct)")
                .onChange(of: self.forceValue) {
                    newValue in
                    if newValue != 0.0 {
                        self.pct = (newValue * 100) / 666
                    }
                }

            ActivityRingView(progress: $pct )
                .padding()
            Spacer()
            HStack {
                CustomView(tappedForceValue: $forceValue)
                    .frame(height: 100)
            }
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



struct CustomView: UIViewRepresentable {
    @Binding var tappedForceValue: CGFloat
    func makeUIView(context: Context) -> ForceButton {
        // Return the UIView object
        let forceButton = ForceButton(type: .roundedRect)
        forceButton.setTitleColor(UIColor.yellow, for: UIControl.State.normal)
        forceButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 35)
        forceButton.backgroundColor = UIColor.blue
        forceButton.setTitle("Force", for: UIControl.State.normal)
        forceButton.frame = CGRect(x: 10, y: 50, width: 100, height: 50)
        forceButton.addTarget(
            context.coordinator,
            action: #selector(Coordinator.updateCurrentPage(sender:)),
            for: .allTouchEvents)
        return forceButton
    }

    func updateUIView(_ forceButton:ForceButton, context: Context) {
        forceButton.forceVal = tappedForceValue
    }

    class Coordinator: NSObject {
        var customView: CustomView

        init(_ customView: CustomView) {
            self.customView = customView
        }

        @objc func updateCurrentPage(sender: ForceButton) {
            customView.tappedForceValue = sender.forceVal
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

}
